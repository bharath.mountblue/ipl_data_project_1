//Number of matches played per year for all the years in IPL.
noOfMatchesPerYear = matches => {
    return matches.reduce((matchesPlayed, matchesElement) => {
        let year = matchesElement.season;
        matchesPlayed[year] = (matchesPlayed[year] || 0) + 1;
        return matchesPlayed;
    }, {});
};

//Number of matches won of per team per year in IPL.
noOfMatchesWonPerTeam = matches => {
    return matches.reduce((matchesWon, match) => {
        let year = match.season;
        let team = match.winner;
        if (matchesWon[year] === undefined) {
            matchesWon[year] = {};
        }
        matchesWon[year][team] = (matchesWon[year][team] || 0) + 1;
        return matchesWon;
    }, {});
};

//Extra runs conceded per team in 2016.
extraRunsConceded = (matches, deliveries, year) => {
    const matchIds = matches
        .filter(match => match.season === year)
        .reduce((ids, match) => {
            ids[match.id] = true;
            return ids;
        }, {});
    return deliveries
        .filter(bowling => bowling.match_id in matchIds)
        .reduce((extraRuns, bowl) => {
            bowlTeam = bowl.bowling_team;
            extraRuns[bowlTeam] = (extraRuns[bowlTeam] || 0) + parseInt(bowl.extra_runs);
            return extraRuns;
        }, {});
};

//Top 10 economical bowlers in 2015.
topTenBowlers = (matches, deliveries, year) => {
    const matchIds = matches
        .filter(match => match.season === year)
        .reduce((ids, match) => {
            ids[match.id] = true;
            return ids;
        }, {});
    const bowlerStatistics = deliveries
        .filter(bowling => bowling.match_id in matchIds)
        .reduce((bowlerBalls, bowl) => {
            let bowlerName = bowl.bowler;
            if (bowlerBalls[bowlerName] === undefined) {
                bowlerBalls[bowlerName] = {};
            }
            bowlerBalls[bowlerName].totalRuns =
                (bowlerBalls[bowlerName].totalRuns || 0) +
                (parseInt(bowl.total_runs) - parseInt(bowl.bye_runs) - parseInt(bowl.legbye_runs));
            bowlerBalls[bowlerName].totalBalls =
                (bowlerBalls[bowlerName].totalBalls || 0) +
                (1 - (parseInt(bowl.wide_runs) - parseInt(bowl.noball_runs)));
            return bowlerBalls;
        }, {});

    const economyInArrangingOrder = Object.entries(bowlerStatistics)
        .filter(bowl => {
            return bowl[1].totalBalls > 12;
        })
        .reduce((bowlerEconomy, bowler) => {
            let economy = bowler[1].totalRuns / (bowler[1].totalBalls / 6);
            bowlerEconomy.push([bowler[0], economy]);
            return bowlerEconomy;
        }, [])
        .sort(function(current, next) {
            return current[1] - next[1];
        });

    const topTen = {};
    for (let i = 0; i < 10; i++) {
        topTen[economyInArrangingOrder[i][0]] = economyInArrangingOrder[i][1];
    }
    return topTen;
};

module.exports = { noOfMatchesPerYear, noOfMatchesWonPerTeam, extraRunsConceded, topTenBowlers };
