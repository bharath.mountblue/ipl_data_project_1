const csvJson = require("csvtojson");
const fileSystem = require("fs");
const iplMatches = require("./ipl");

outputToJson = (fileDestination, output) => {
    fileSystem.writeFile(fileDestination, JSON.stringify(output, null, 2), "utf8", err => {
        if (err) throw err;
        console.log("Saved");
    });
};

csvJson()
    .fromFile("./src/data/matches.csv")
    .then(matches => {
        //Number of matches played per year for all the years in IPL.
        output = iplMatches.noOfMatchesPerYear(matches);
        fileDestination = "./src/output/matchesPerYear.json";
        outputToJson(fileDestination, output);

        //Number of matches won of per team per year in IPL.
        output = iplMatches.noOfMatchesWonPerTeam(matches);
        fileDestination = "./src/output/matchesPerTeamWon.json";
        outputToJson(fileDestination, output);

        csvJson()
            .fromFile("./src/data/deliveries.csv")
            .then(deliveries => {
                //Extra runs conceded per team in 2016.
                year = "2016";
                output = iplMatches.extraRunsConceded(matches, deliveries, year);
                fileDestination = "./src/output/extrasPerTeam.json";
                outputToJson(fileDestination, output);

                //Top 10 economical bowlers in 2015.
                year = "2015";
                output = iplMatches.topTenBowlers(matches, deliveries, year);
                fileDestination = "./src/output/topEconomicBowler.json";
                outputToJson(fileDestination, output);
            });
    });
