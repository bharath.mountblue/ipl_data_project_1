const http = require("http");
const fs = require("fs");
const path = require("path");
const port = 8080;
const server = http.createServer((req, res) => {
    switch (req.url) {
        case "/":
            fs.readFile("./index.html", "UTF-8", (error, data) => {
                res.writeHead(200, { "Content-Type": "text/html" });
                res.end(data);
            });
            break;
        case "/app.js":
            let chartsPath = path.join(__dirname, req.url);
            let fileStream1 = fs.createReadStream(chartsPath, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/javascript" });
            fileStream1.pipe(res);
            break;
        case "/matchesPerYear.json":
            let jsonPath1 = path.join("../output", req.url);
            let fileStream2 = fs.createReadStream(jsonPath1, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/json" });
            fileStream2.pipe(res);
            break;
        case "/matchesPerTeamWon.json":
            let jsonPath2 = path.join("../output", req.url);
            let fileStream3 = fs.createReadStream(jsonPath2, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/json" });
            fileStream3.pipe(res);
            break;
        case "/extrasPerTeam.json":
            let jsonPath3 = path.join("../output", req.url);
            let fileStream4 = fs.createReadStream(jsonPath3, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/json" });
            fileStream4.pipe(res);
            break;
        case "/topEconomicBowler.json":
            let jsonPath4 = path.join("../output", req.url);
            let fileStream5 = fs.createReadStream(jsonPath4, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/json" });
            fileStream5.pipe(res);
            break;
        case "/app.css":
            let cssPath = path.join(__dirname, req.url);
            let fileStream6 = fs.createReadStream(cssPath, "UTF-8");
            res.writeHead(200, { "Content-Type": "text/css" });
            fileStream6.pipe(res);
            break;
    }
});
server.listen(port, error => {
    if (error) {
        console.log(`Something went wrong ${error}`);
    } else {
        console.log(`Listening on port ${port}`);
    }
});