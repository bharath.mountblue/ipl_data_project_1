const charts = (data, input) => {
    Highcharts.chart(input.id, {
        chart: {
            type: input.type
        },
        title: {
            text: input.title
        },
        xAxis: {
            title: {
                text: input.xTitle
            },
            categories: Object.keys(data)
        },
        yAxis: {
            title: {
                text: input.yTitle
            }
        },
        series: input.series
    });
};

fetch("matchesPerYear.json")
    .then(response => {
        return response.json();
    })
    .then(data => {
        const input = {
            id: chart1,
            type: "line",
            title: "Number of matches played per year in IPL",
            xTitle: "Years",
            yTitle: "No of matches",
            series: [
                {
                    name: "Matches",
                    data: Object.values(data)
                }
            ]
        };
        charts(data, input);
    });

fetch("matchesPerTeamWon.json")
    .then(response => {
        return response.json();
    })
    .then(data => {
        let index = 0;
        const seriesInput = Object.values(data).reduce((team, element) => {
            for (name in element) {
                teamName = name;
                if (teamName === "Deccan Chargers") {
                    teamName = "Sunrisers Hyderabad";
                } else if (teamName === "Pune Warriors" || teamName === "Rising Pune Supergiant") {
                    teamName = "Rising Pune Supergiants";
                }
                team[teamName] = team[teamName] || {
                    name: teamName,
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                };
                team[teamName].data[index] =
                    team[teamName].data[index] + (element[teamName] || element[name]);
            }
            index++;
            return team;
        }, {});

        const input = {
            id: chart2,
            type: "column",
            title: "Number of matches won of per team per year in IPL.",
            xTitle: "Years",
            yTitle: "No of matches won",
            series: Object.values(seriesInput)
        };
        charts(data, input);
    });

fetch("extrasPerTeam.json")
    .then(response => {
        return response.json();
    })
    .then(data => {
        const input = {
            id: chart3,
            type: "column",
            title: "Extra runs conceded per team in 2016.",
            xTitle: "Teams",
            yTitle: "Extra runs",
            series: [
                {
                    name: "Extra Runs",
                    data: Object.values(data)
                }
            ]
        };
        charts(data, input);
    });

fetch("topEconomicBowler.json")
    .then(response => {
        return response.json();
    })
    .then(data => {
        const input = {
            id: chart4,
            type: "line",
            title: "Top 10 economical bowlers in 2015.",
            xTitle: "BowlerName",
            yTitle: "Economy",
            series: [
                {
                    name: "Economy",
                    data: Object.values(data)
                }
            ]
        };
        charts(data, input);
    });
