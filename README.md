ipl_data_project_1

1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016.
4. Top 10 economical bowlers in 2015.


Directory structure:

    src/
        server/
            ipl.js
            index.js
        output/
            matchesPerYear.json
            extrasPerTeam.json
            matchesPerYear.json
            topEconomicBowler.json
        data/
            matches.csv
            deliveries.csv
    package.json
    package-lock.json
    .gitignore
    README.md



